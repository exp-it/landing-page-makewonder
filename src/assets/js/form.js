class Validator {
    min(content, min) {        
        if(content === undefined || content == null) return false
        return content.length >= min
    }
 
    max(content, max) {
        if(content === undefined || content == null) return false
        return content.length <= max
    }
 
    email(content) {
        if(content === undefined) return false
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(content).toLowerCase());
    }
}


class App extends React.Component {
    constructor(props) {
        super(props)

        this.validator = new Validator()

        this.state = {
            email: null,
            checkbox: false
        }

        this.handleSubmit = this.handleSubmit.bind(this)
    }

    hasErrors(){
        if(!this.validator.email(this.state.email)) return false
        if(!this.state.checkbox) return false
        return true
    }

    handleSubmit() {
        fetch('https://cms.makewonder.pl/wp-json/ex/newsletter', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: this.state.email
            })
        }).then(() => {
            alert('Dziękujemy, oczekuj na wiadomość email')
        })
    }

    render() {
        return (
            <div>
                <label className="field">
                    <i className="icon-message"></i>
                    <input
                    type="email"
                    onChange={(e) => {
                        this.setState({email: e.target.value})
                    }}
                    name="email"
                    className={this.state.email ? null : 'empty' }
                    required />
                    <span className="placeholder">Podaj e-mail</span>
                </label>

                <label className="terms">
                    <input 
                        type="checkbox" 
                        onChange={(e) => {
                            this.setState({checkbox: !this.state.checkbox})
                        }} 
                        name="term"
                        checked={this.state.checkbox} 
                        required />
                    <div>
                        <div>
                            <div className="checkmark"></div>
                        </div>
                        <p>Zapoznałem się z <a href="https://wonderpolska.pl/POLITYKA-PRYWATNOSCI-I-OCHRONA-DANYCH-OSOBOWYCH-cterms-pol-20.html" target="_blank">polityką prywatności</a> i wyrażam zgodę na przetwarzanie moich danych osobowych w celu informowania o publikacjach i projektach edukacyjnych.</p>
                    </div>
                </label>

                <button type="button" onClick={this.handleSubmit} className={this.hasErrors() ? 'button left-icon active' : 'button left-icon'}><i className="icon-download"></i><span>Pobierz darmowy scenariusz</span></button>
            </div>
        )
    }
}


const el = document.getElementById('form')
ReactDOM.render(<App></App>, el)