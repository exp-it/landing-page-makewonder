import $ from 'jquery';
import 'what-input';
import Typed from 'typed.js';
import AOS from 'aos';

// Foundation JS relies on a global varaible. In ES6, all imports are hoisted
// to the top of the file so if we used`import` to import Foundation,
// it would execute earlier than we have assigned the global variable.
// This is why we have to use CommonJS require() here since it doesn't
// have the hoisting behavior.
window.jQuery = $;
require('foundation-sites');

// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';


$(document).foundation();

//aos
AOS.init();

//type
var typed = new Typed('#typed', {
  stringsElement: '#typed-strings',
  startDelay: 400,
  typeSpeed: 80,
  backDelay: 2200,
  loop: true,
  fadeOut: true,
  fadeOutDelay: 400,
  showCursor: false
});


//error
window.addEventListener("DOMContentLoaded", function(){
  // Check if browser is supported
  if (window.navigator.userAgent.indexOf("Trident/") > 0){
        document.getElementById("error-browser").style.display = "block";
  }
})